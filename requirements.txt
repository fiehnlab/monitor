requests
simplejson
yamlconf
watchdog
boto3
botocore
loguru
pywin32
retry
git+https://github.com/metabolomics-us/carpy.git@master#egg=stasis-client&subdirectory=stasis-client
git+https://github.com/metabolomics-us/carpy.git@master#egg=cis-client&subdirectory=cis-client
